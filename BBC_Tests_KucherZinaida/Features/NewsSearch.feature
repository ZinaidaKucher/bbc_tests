﻿Feature: NewsSearch
	In order to check the displaying of corect titles  
	As a user of BBC site
	I want to be sure that there is displaying correct data

@mainNews
Scenario: Check the title of the main article on News Page
	Given the https://www.bbc.com site is opened	
	When I click to News tab
	Then the name of headline article should be 'Kim 'apologises for killing of South Korean official''

@secondaryNews
Scenario: Check the titles of the secondary articles on News Page
	Given the https://www.bbc.com site is opened	
	When I click to News tab
	Then the names of secondary articles should be 
	| values                                        |
	| Global warming 'driving California wildfires' |
	| Vatican cardinal resigns unexpectedly         |
	| Rio carnival 2021 postponed due to Covid      |
	| Are the lockdown restrictions too strict?     |
	| Mine-detecting rat wins animal bravery award  |




@categorySearch
Scenario: Check the name of the first article in acordance with category
	Given the https://www.bbc.com site is opened	
	When I click to News tab
	And I search text of category link of the headline article through Search Bar	
	Then the name of headline article should contains 'Asia' 