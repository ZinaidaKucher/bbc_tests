﻿Feature: SendQuestionToBBC
	In order to verify that user cannot submit a question to BBC with invalid data
	As a User
	I want to submit a question to BBC and receive error message

@mytag
Scenario Outline: Send Question To BBC with parameters
Given the https://www.bbc.com site is opened
When I fill the Form on the Share With BBC News Page with the data and click Submit button
| fields                 | value                    |
| Tell us your story     | <Tell us your story>     |
| Name                   | <Name>                   |
| I am over 16 years old | <I am over 16 years old> |
| I accept               | <I accept>               |

Then the error message <Error message> is displayed

Examples: 
| Tell us your story | Name | I am over 16 years old | I accept | Error message    |
|                    | Zina | Yes                    | Yes      | can't be blank   |
| Story              |	    | Yes                    | Yes      | can't be blank   |
| Story              | Zina | No                     | Yes      | must be accepted |
| Story              | Zina | Yes                    | No       | must be accepted |