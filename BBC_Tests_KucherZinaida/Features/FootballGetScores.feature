﻿Feature: FootballGetScores
	In order to verify that score of teams is displaying accurate
	As a User
	I want to check the score of the given teams

@mytag
Scenario Outline: Searching score of the teams
	Given the https://www.bbc.com site is opened	
	When I search teams <NameOfFirstTeam> and <NameOfSecondTeam> which played in <Championship> in <Month> on the Scores & Fixtures page
	Then the teams played with the score: <NameOfFirstTeam>- <Score>- <NameOfSecondTeam>
	When I click to the <NameOfFirstTeam>
	Then new page with the same teams and score is displayed: <NameOfFirstTeam>- <Score>- <NameOfSecondTeam>
	Examples: 
	| NameOfFirstTeam      | NameOfSecondTeam    | Championship                             | Month   | Score |
	| Manchester United    | Chelsea             | THE FA CUP                               | 2020-07 | 1-3   |
	| Norway               | Wales               | WOMEN'S EUROPEAN CHAMPIONSHIP QUALIFYING | 2020-09 | 1-0   |
	| Germany              | Republic of Ireland | WOMEN'S EUROPEAN CHAMPIONSHIP QUALIFYING | 2020-09 | 3-0   |
	| Celtic               | Ferencvárosi TC     | CHAMPIONS LEAGUE                         | 2020-08 | 1-2   |
	| Liverpool            | Atlético Madrid     | CHAMPIONS LEAGUE                         | 2020-03 | 2-3   |
	| VfL Wolfsburg Ladies | Lyon Féminines      | WOMEN'S CHAMPIONS LEAGUE                 | 2020-08 | 1-3   |

