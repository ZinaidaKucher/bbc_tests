﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using FindsByAttribute = SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace BBC_Tests_KucherZinaida
{
    public class BaseObject
    {
       private WebDriverWait wait;

        public BaseObject()
        {
            wait = new WebDriverWait(GetDriver.Driver, TimeSpan.FromSeconds(5));
            PageFactory.InitElements(GetDriver.Driver, this);
        }

    }
}
