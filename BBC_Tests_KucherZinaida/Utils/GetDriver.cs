﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_Tests_KucherZinaida.Utils
{
    public class GetDriver
    {
        private static IWebDriver driver;
        public static IWebDriver Driver
        {
            get
            {
                if (driver == null)
                {
                    driver = new ChromeDriver();
                }
                return driver;
            }
            set
            { driver = value; }
        }

        public static void CloseGetDriver()
        {
            Driver.Close();
            Driver = null;
        }

    }
    
}
