﻿using OpenQA.Selenium;
using System.Collections.Generic;
using BBC_Tests_KucherZinaida.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BBC_Tests_KucherZinaida.Utils
{
    public class Form
    {
        private string inputs;

        public void FillForm(Dictionary<string, string> values)
        {
            var driver = GetDriver.Driver;
            Waiters.ImplicitWait(0);

            foreach (KeyValuePair<string, string> inputField in values)
            {
                inputs = inputField.Key;
                Assert.IsTrue(inputs.Contains("Tell us your story") || inputs.Equals("Name") || inputs.Equals("I am over 16 years old") || inputs.Equals("I accept"), "Error - such field is not exist. Field name should be 'Tell us your story' or 'Name' or 'I am over 16 years old' or 'I accept'");

                if (driver.FindElements(By.XPath("//div[contains(@class,'content-container')]//*[contains(@aria-label,'" + inputs + "')]")).Count == 0)
                {
                    IWebElement checkboxPath = driver.FindElement(By.XPath("//div[contains(@class,'content-container')]//*[contains(text(),'" + inputs + "')]"));
                    if (inputField.Value == "Yes")
                        checkboxPath.Click();
                }
                else
                {
                    IWebElement fieldPath = driver.FindElement(By.XPath("//div[contains(@class,'content-container')]//*[contains(@aria-label,'" + inputs + "')]"));
                    fieldPath.SendKeys(inputField.Value);
                }
            }
        }
    }
 }
