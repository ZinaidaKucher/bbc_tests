﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using FindsByAttribute = SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class SignInExit : BaseObject
    {
        public SignInExit() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//button[@class='sign_in-exit']")]
        private IWebElement SignInExitButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='sign_in-container']")]
        private IList<IWebElement> SignInPopUp;

        public void ClickOnSignInExit()
        {
            Waiters.ImplicitWait(5);
            if (SignInPopUp.Count != 0)       
            {
                SignInExitButton.Click();
            }
        }
    }
}

