﻿using OpenQA.Selenium;
using System.Collections.Generic;
using SeleniumExtras.PageObjects;
using System;
using FindsByAttribute = SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace BBC_Tests_KucherZinaida.Utils
{

    public class Score
    {
        public string ScoreOfFirstTeam ;
        public string ScoreOfSecondTeam;
        public string TotalScore;

        public Score(string score1, string score2)
        {            
            ScoreOfFirstTeam = score1;
            ScoreOfSecondTeam = score2;
            TotalScore = String.Format("{0}-{1}", ScoreOfFirstTeam, ScoreOfSecondTeam);           
        }
    }

    public class ScoreBoard : BaseObject
    {
        public ScoreBoard() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'fixture__wrapper')]")]
        private IList<IWebElement> teamsLines;

        public Score GetScore(string team1, string team2)
        {
            var driver = GetDriver.Driver;
            Waiters.ImplicitWait(1);


            List<string> scoreList = new List<string>();

                for (int i = 0; i < teamsLines.Count; i++)
            {
                IWebElement teamsLinesList = driver.FindElement(By.XPath("(//div[contains(@class,'fixture__wrapper')])["+(i+1)+"]"));
                if (driver.FindElements(By.XPath("(//div[contains(@class,'fixture__wrapper')])[" + (i + 1) + "]//span[contains(text(),'" + team1 + "')]")).Count != 0 && driver.FindElements(By.XPath("(//div[contains(@class,'fixture__wrapper')])[" + (i + 1) + "]//span[contains(text(),'" + team2 + "')]")).Count != 0)
                {
                    scoreList.Add(teamsLinesList.FindElement(By.XPath("//span[contains(text(),'" + team1 + "')]//ancestor::span[contains(@class,'fixture__team--home')]//span[contains(@class,'number')]")).Text);
                    scoreList.Add(teamsLinesList.FindElement(By.XPath("//span[contains(text(),'" + team2 + "')]//ancestor::span[contains(@class,'fixture__team--away')]//span[contains(@class,'number')]")).Text);
                }                
            }
            string score1 = scoreList[0];
            string score2 = scoreList[1];                
            return new Score(score1, score2);          
        }
    }
}





