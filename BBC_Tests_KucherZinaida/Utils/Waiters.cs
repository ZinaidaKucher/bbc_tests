﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBC_Tests_KucherZinaida.Utils
{
    class Waiters
    {
        public static void ImplicitWait(int timeToWait)
        {
           GetDriver.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeToWait);
        }

        public static void WaitForPageLoadComplete(int timeToWait)
        {
            WebDriverWait wait = new WebDriverWait(GetDriver.Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

    }
}
