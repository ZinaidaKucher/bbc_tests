﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using FindsByAttribute = SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class HomePage:BaseObject
    {
        public HomePage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//nav[@role='navigation']//li[contains(@class,'news')]")]
        private IWebElement NewsButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'nav-section')]//li[@class='orb-nav-sport']")]
        private IWebElement SportButton;

        public SignInExit ClickOnNewsButton()
        {
            Waiters.WaitForPageLoadComplete(10);
            NewsButton.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new SignInExit();
        }

        public SignInExit ClickOnSportButton()
        {
            Waiters.WaitForPageLoadComplete(10);
            SportButton.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new SignInExit();
        }
    }
}
