﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class SportPage : HomePage
    {
        public SportPage() : base()
        { }
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'navigation__inner')]//a[@data-stat-title='Football']")]
        private IWebElement FootballTab;

        public FootballPage ClickToFootballTab()
        {
            FootballTab.Click();
            Waiters.WaitForPageLoadComplete(5);
            return new FootballPage();
        }
    }
}
