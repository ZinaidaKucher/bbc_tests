﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class FootballPage : HomePage
    {
        public FootballPage() : base()
        { }
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'navigation__inner')]//a[@data-stat-title='Scores & Fixtures']")]
        private IWebElement ScoresAndFixturesTab;

        public ScoresAndFixturesPage ClickToScoresAndFixturesTab()
        {
            ScoresAndFixturesTab.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new ScoresAndFixturesPage();
        }

    }
}
