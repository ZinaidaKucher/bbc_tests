﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class CoronavirusPage : HomePage
    {
        public CoronavirusPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'secondary')]//a[contains(@href,'/news/have_your_say')]")]
        private IWebElement YourCoronavirusStoriesTab;

        public YourCoronavirusStoriesPage ClickToYourCoronavirusStoriesTab()
        {
            YourCoronavirusStoriesTab.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new YourCoronavirusStoriesPage();
        }
    }
}
