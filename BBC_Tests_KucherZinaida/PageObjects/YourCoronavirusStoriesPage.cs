﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;


namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class YourCoronavirusStoriesPage : HomePage
    {
        public YourCoronavirusStoriesPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//a[@href='/news/10725415']")]
        private IWebElement ShareWithBBCNewsTab;

        public ShareWithBBCNewsPage ClickToShareWithBBCNewsTab()
        {
            ShareWithBBCNewsTab.Click();
            return new ShareWithBBCNewsPage();
        }

        public ShareWithBBCNewsPage ClickToShareWithBBCNewsWithForm()
        {
            ShareWithBBCNewsTab.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new ShareWithBBCNewsPage();
        }
    }
}
