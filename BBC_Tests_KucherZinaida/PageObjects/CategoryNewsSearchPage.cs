﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class CategoryNewsSearchPage : HomePage
    {
        public CategoryNewsSearchPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//p[contains(@class,'PromoHeadline')]")]
        private IWebElement NameOfArticle;

        public string TextNameOfFirstArticle()
        {
            return NameOfArticle.Text;
        }

    }
}
