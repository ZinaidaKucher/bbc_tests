﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class ShareWithBBCNewsPage : HomePage
    {
        public ShareWithBBCNewsPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//button[@class='button']")]
        private IWebElement SubmitButton;

  
        [FindsBy(How = How.XPath, Using = "//div[@class='input-error-message']")]
        private IWebElement ErrorMessage;

        public Form GetFormToShareWithBBCNewsWithForm()
        {
            return new Form();
        }

        public void ClickSubmitButton()
        {
            SubmitButton.Click();
            Waiters.ImplicitWait(2);
        }

        public string GetErrorMessageText()
        {
            return ErrorMessage.Text;
        }
    }
}
