﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
   public class ChampoinshipScoresAndFixturesPage : HomePage
    {
        public ChampoinshipScoresAndFixturesPage() : base()
        { }
       
        public ChampoinshipOfMonthScoresAndFixturesPage SearchChampionshipOfMonth(string yearMonth)
        {
            var driver = GetDriver.Driver;
            IWebElement monthField = driver.FindElement(By.XPath("//a[contains(@href,'" + yearMonth + "')]"));
            monthField.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new ChampoinshipOfMonthScoresAndFixturesPage();
        }

    }
}
