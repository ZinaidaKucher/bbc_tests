﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class ChampoinshipOfMonthScoresAndFixturesPage : HomePage
    {
        public ChampoinshipOfMonthScoresAndFixturesPage() : base()
        { }

        public ScoreBoard GetScoreBoard()
        {
            return new ScoreBoard();
        }

        public SpecifiedGamePage SearchChampionshipOfMonth(string team1)
        {
            Waiters.ImplicitWait(2);
            var driver = GetDriver.Driver;
            IWebElement NameOfFirstTeam = driver.FindElement(By.XPath("//span[contains(text(),'" + team1 + "')]"));
            NameOfFirstTeam.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new SpecifiedGamePage();
        }

    }

}
