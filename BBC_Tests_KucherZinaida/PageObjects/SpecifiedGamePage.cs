﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
   public class SpecifiedGamePage : HomePage
    {
        public SpecifiedGamePage() : base()
        { }

        public ScoreBoard GetScoreBoard()
        {
            return new ScoreBoard();
        }
    }
}
