﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using FindsByAttribute = SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace BBC_Tests_KucherZinaida.PageObjects
{
    public class NewsPage : HomePage
    {
        public NewsPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'top-stories__primary')]//div[contains(@class,'block@m')]//h3[contains(@class,'heading__title')]")]
        private IWebElement NameOfArticle;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'top-stories__secondary-item')]//h3[contains(@class,'heading__title')]")]
        private IList<IWebElement> NameOfSecondaryArticlesList;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'top-stories__primary')]//div[contains(@class,'block@m')]//a[contains(@class,'section-link')]//span[@aria-hidden='true']")]
        private IWebElement CategoryLink;

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement SearchBar;

        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        private IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'news-wide-navigation')]//a[contains(@href,'coronavirus')]")]
        private IWebElement CoronavirusTab;


        public bool Search(string toSearch)
        {
            return NameOfArticle.Text == toSearch;
        }

        public List<string> SearchResultArticles()
        {
            List<string> myList = new List<string>();
            foreach (IWebElement webElement in NameOfSecondaryArticlesList)
            {
                string elementText = webElement.Text;
                myList.Add(elementText);
            }
            return myList;
        }

        public string SearchResultCategoryLinkText()
        {
            return CategoryLink.Text;
        }

        public CategoryNewsSearchPage SendRequestWithCategoryLinkToSearchBar()
        {
            string CategoryLinkText = CategoryLink.Text;
            SearchBar.SendKeys(CategoryLinkText);
            SearchButton.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new CategoryNewsSearchPage();
        }


        public CoronavirusPage ClickToCoronavirusTab()
        {
            CoronavirusTab.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new CoronavirusPage();
        }
    }
}
