﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBC_Tests_KucherZinaida.PageObjects
{
   public class ScoresAndFixturesPage : HomePage
    {
        public ScoresAndFixturesPage() : base()
        { }

        [FindsBy(How = How.XPath, Using = "//input[@name='search']")]
        private IWebElement SearchField;

        [FindsBy(How = How.XPath, Using = "//div[@id='search-results']")]
        private IWebElement SearchResultDropdown;

        public void SearchChampionshipThroughSearchField(string Championship)
        {
           SearchField.SendKeys(Championship);
        }

        public ChampoinshipScoresAndFixturesPage ClickToSearchResultDropdown()
        {
            SearchResultDropdown.Click();
            Waiters.WaitForPageLoadComplete(10);
            return new ChampoinshipScoresAndFixturesPage();
        }

    }
}
