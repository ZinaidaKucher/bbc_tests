﻿using BBC_Tests_KucherZinaida.PageObjects;
using BBC_Tests_KucherZinaida.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace BBC_Tests_KucherZinaida.Features
{
    [Binding]
    public class SendQuestionToBBCSteps : Hooks
    {
     
        [When(@"I fill the Form on the Share With BBC News Page with the data and click Submit button")]
        public void WhenIFillTheFormOnTheShareWithBBCNewsPageWithTheDataAndClickSubmitButton(Table table)
        {
            new HomePage().ClickOnNewsButton();        
            SignInExit exit = new SignInExit();
            exit.ClickOnSignInExit();          
            CoronavirusPage coronavirus = new NewsPage().ClickToCoronavirusTab();
            coronavirus.ClickToYourCoronavirusStoriesTab();
            ShareWithBBCNewsPage shareNews = new YourCoronavirusStoriesPage().ClickToShareWithBBCNewsWithForm();
            var dictionary = TableExtensions.ToDictionary(table);          
            Form form = shareNews.GetFormToShareWithBBCNewsWithForm();
            form.FillForm(dictionary);         
            shareNews.ClickSubmitButton();
        }

        [Then(@"the error message (.*) is displayed")]
        public void ThenTheErrorMessageIsDisplayed(string p0)
        {
            ShareWithBBCNewsPage share = new ShareWithBBCNewsPage();
            Assert.IsTrue(share.GetErrorMessageText().Contains(p0));           
        }
    }
}
