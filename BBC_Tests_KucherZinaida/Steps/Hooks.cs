﻿using BBC_Tests_KucherZinaida.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace BBC_Tests_KucherZinaida.Features
{
    [Binding]
    public class Hooks
    {
        [BeforeScenario]
        public static void BeforeScenario()
        {
            GetDriver.Driver.Manage().Window.Maximize();   
        }

        [AfterScenario]
        public static void AfterScenario()
        {
           GetDriver.CloseGetDriver();
        }
    }
}
