﻿using BBC_Tests_KucherZinaida.PageObjects;
using BBC_Tests_KucherZinaida.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Net.Http.Headers;
using TechTalk.SpecFlow;

namespace BBC_Tests_KucherZinaida.Features
{
    [Binding]
    public class NewsSearchSteps:Hooks
    {
        [Given(@"the (.*) site is opened")]
        public void GivenTheBbcSiteIsOpened(string BBC_URL)
        {                   
           GetDriver.Driver.Navigate().GoToUrl(BBC_URL);
        }

        [When(@"I click to News tab")]
        public void WhenIClickToNewsTab()
        {          
            new HomePage().ClickOnNewsButton();
            SignInExit exit = new SignInExit();
            exit.ClickOnSignInExit();
        }

        [Then(@"the name of headline article should be '(.*)'")]
        public void ThenTheNameOfHeadlineArticleShouldBe(string EXPECTED_NAME_OF_MAIN_ARTICLE)
        {
            NewsPage news = new NewsPage();
            Assert.IsTrue(news.Search(EXPECTED_NAME_OF_MAIN_ARTICLE));
        }

        [Then(@"the names of secondary articles should be")]
        public void ThenTheNamesOfSecondaryArticlesShouldBe(Table table)
        {
            NewsPage news = new NewsPage();
            var list = TableExtensions.ToList(table);
            Assert.IsTrue(news.SearchResultArticles().SequenceEqual(list));
        }

        [When(@"I search text of category link of the headline article through Search Bar")]
        public void WhenISearchTextOfCategoryLinkOfTheHeadlineArticleThroughSearchBar()
        {
            new NewsPage().SendRequestWithCategoryLinkToSearchBar();
        }

        [Then(@"the name of headline article should contains '(.*)'")]
        public void ThenTheNameOfHeadlineArticleShouldContains(string categoryLink)
        {
            CategoryNewsSearchPage search = new CategoryNewsSearchPage();
            string text = search.TextNameOfFirstArticle();
            Assert.IsTrue(text.Contains(categoryLink));
        }

    }
}
