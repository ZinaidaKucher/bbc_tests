﻿using BBC_Tests_KucherZinaida.PageObjects;
using BBC_Tests_KucherZinaida.Utils;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace BBC_Tests_KucherZinaida.Features
{
    [Binding]
    public class FootballGetScoresSteps
    {
        [When(@"I search teams (.*) and (.*) which played in (.*) in ([0-9]{4}-[0-9]{2}.*) on the Scores & Fixtures page")]
        public void WhenISearchTeamsAndWhichPlayedInInOnTheScoresFixturesPage(string team1, string team2, string championship, string yearMonth)
        {
            new HomePage().ClickOnSportButton();         
            SignInExit exit = new SignInExit();
            exit.ClickOnSignInExit();
            FootballPage football = new SportPage().ClickToFootballTab();          
            ScoresAndFixturesPage champ = football.ClickToScoresAndFixturesTab();           
            champ.SearchChampionshipThroughSearchField(championship);
            ChampoinshipScoresAndFixturesPage champPage = champ.ClickToSearchResultDropdown();
            champPage.SearchChampionshipOfMonth(yearMonth);    
        }

        [Then(@"the teams played with the score: (.*)- (.*)- (.*)")]
        public void ThenTheTeamsPlayedWithTheScore(string team1, string score, string team2)
        {
            ScoreBoard scoreBoard = new ChampoinshipOfMonthScoresAndFixturesPage().GetScoreBoard();                   
            Assert.AreEqual(score, scoreBoard.GetScore(team1, team2).TotalScore);
        }

        [When(@"I click to the (.*)")]
        public void WhenIClickToThe(string teamFirstName)
        {
            new ChampoinshipOfMonthScoresAndFixturesPage().SearchChampionshipOfMonth(teamFirstName);
        }

        [Then(@"new page with the same teams and score is displayed: (.*)- (.*)- (.*)")]
        public void ThenNewPageWithTheSameTeamsAndScoreIsDisplayed(string team1, string score, string team2)
        {
            ScoreBoard scoreBoard = new SpecifiedGamePage().GetScoreBoard();
            Assert.IsTrue(scoreBoard.GetScore(team1, team2).TotalScore.SequenceEqual(score));
        }
    }
}
